(function () {

    'use strict';

    angular
        .module('giphy', [
            'angular-loading-bar',
            'core',
            'toastr',
            'ui.router',
            'ngAnimate',
            'ngMaterial',
            'ngMdIcons',
            'ngMessages',
            'ngStorage',
        ])
        .run(runBlock);

    /** @ngInject */
    function runBlock($localStorage) {
        if (!$localStorage.userHolyGiphy) {
            $localStorage.userHolyGiphy = [];
        }
    }

})();