(function () {

    'use strict';

    angular
        .module('giphy')
        .controller('BigSizeGiphyController', BigSizeGiphyController);

    /** @ngInject */

    function BigSizeGiphyController($mdDialog, item) {
        var vm = this;

        vm.item = item;
        vm.closeDialog = closeDialog;

        function closeDialog() {
            $mdDialog.hide();
        }
    }
})();