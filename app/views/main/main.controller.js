(function () {

    'use strict';

    angular
        .module('giphy')
        .controller('Main', Main);

    /** @ngInject */

    function Main($mdDialog, $document, $scope, gif, user) {
        var vm = this;

        vm.textSearch = '';
        vm.stockList = [];
        vm.sliceSearch = 21;


        vm.loadGifs = loadGifs;
        vm.loadGifsPlus = loadGifsPlus;
        vm.openGif = openGif;
        vm.addGifInCollection = addGifInCollection;

        function loadGifs(tag) {
            for (var i = 0; i < vm.sliceSearch; i++) {
                gif.randomGifs({
                    tag: tag
                }, function (res) {
                    vm.stockList.push(res.data);
                });

            }
        }

        function loadGifsPlus(tag) {
            for (var i = 0; i < 3; i++) {
                gif.randomGifs({
                    tag: tag
                }, function (res) {
                    vm.stockList.push(res.data);
                });
            }
        }

        function openGif(item, e) {
            $mdDialog.show({
                controller: 'BigSizeGiphyController',
                controllerAs: 'vm',
                templateUrl: 'views/main/window/big-size/big-size.html',
                parent: angular.element($document.body),
                targetEvent: e,
                clickOutsideToClose: true,
                locals: {
                    item: item
                }
            });
        }

        function addGifInCollection(id) {
            user.setToCollection(id);
        }

        vm.loadGifs();

        // Watch the model changes to trigger the search
        $scope.$watch('vm.textSearch', function (current, old) {
            if (angular.isUndefined(current)) {
                return;
            }

            if (angular.equals(current, old)) {
                return;
            }

            vm.stockList = [];
            vm.loadGifs(current);
        });
    }
})();