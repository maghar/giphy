(function () {

    'use strict';

    angular
        .module('giphy')
        .controller('UserCollection', UserCollection);

    /** @ngInject */

    function UserCollection(userCollection, gif, user, $mdDialog, $document) {
        var vm = this;

        var joinedCollection = userCollection.join();

        vm.collection = [];

        vm.uploadGif               = uploadGif;
        vm.deleteGifOfCollection   = deleteGifOfCollection;

        function uploadGif(e) {
            $mdDialog.show({
                controller          : 'GiphyUploadController',
                controllerAs        : 'vm',
                templateUrl         : 'views/collection/window/giphy-upload/giphy-upload.html',
                parent              : angular.element($document.body),
                targetEvent         : e,
                clickOutsideToClose : false
            }).then(function () {
                joinedCollection = user.getCollection();
                joinedCollection = joinedCollection.join();
                loadData();
            },function () {
                // cancelled
            });
        }

        function deleteGifOfCollection(id, index) {
            user.removeFromCollection(id);
            vm.collection.splice(index, 1);
        }

        function loadData() {
            gif.gifsId({
                ids: joinedCollection
            }, function (res) {
                vm.collection = res.data;
            });
        }

        loadData();
    }
})();