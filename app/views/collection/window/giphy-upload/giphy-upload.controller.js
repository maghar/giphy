(function () {

    'use strict';

    angular
        .module('giphy')
        .controller('GiphyUploadController', GiphyUploadController);

    /** @ngInject */

    function GiphyUploadController($mdDialog, gif, user, toastr) {
        var vm = this;

        vm.fileName = '';
        vm.userName = '';
        vm.tags = [];

        vm.uploadFileInServer = uploadFileInServer;
        vm.closeDialog = closeDialog;

        function uploadFileInServer() {
            $mdDialog.hide();

            setTimeout(function () { 
                alert(`Sorry but, file uploads are limited to developers Giphy.com!
Need more rights`) 
            }, 500);

        }

        function closeDialog() {
            $mdDialog.cancel();
        }
    }
})();