(function () {

    'use strict';

    angular
        .module('giphy')
        .config(config);

    /** @ngInject */
    function config($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;

        $urlRouterProvider.otherwise('/login');

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'views/login/login.html',
                controller: 'Login',
                controllerAs: 'vm'
            })

            .state('registration', {
                url: '/registration',
                templateUrl: 'views/registration/registration.html',
                controller: 'Registration',
                controllerAs: 'vm'
            })
            
            .state('app', {
                abstract: true,
                templateUrl: 'views/template/template.html',
                controller: 'Shell',
                controllerAs: 'vm'
            })

            .state('app.main', {
                url: '/main',
                templateUrl: 'views/main/main.html',
                controller: 'Main',
                controllerAs: 'vm'
            })
            
            .state('app.collection', {
                url: '/collection',
                templateUrl: 'views/collection/collection.html',
                controller: 'UserCollection',
                controllerAs: 'vm',
                resolve: {
                    userCollection: getUserCollection
                }
            });

        function getUserCollection(user) {
            return user.getCollection();
        }
    }
})();