(function () {

    'use strict';

    angular
        .module('services')
        .service('gif', gif);

    /** @ngInject */
    function gif(http, url) {
        return {
            trendingGifs    : trendingGifs,
            randomGifs      : randomGifs,
            searchGif       : searchGif,
            gifsId          : gifsId,
            uploadGif       : uploadGif
        };

        function trendingGifs(data, callback) {
            return http.get(url.gifs.trendingGifs, data)
                .then(function (res) {
                    callback(res);
                });
        }
  
        function randomGifs(data, callback) {
            return http.get(url.gifs.randomGifs, data)
                .then(function (res) {
                    callback(res);
                });
        }

        function searchGif(data, callback) {
            return http.get(url.gifs.searchGif, data)
                .then(function (res) {
                    callback(res);
                });
        }

        function gifsId(data, callback) {
            return http.get(url.gifs.gifsId, data)
                .then(function (res) {
                    callback(res);
                });
        }

        function uploadGif(data, callback) {
            return http.post(url.gifs.uploadGif, data)
                .then(function (res) {
                    callback(res);
                });
        }
    }
})();