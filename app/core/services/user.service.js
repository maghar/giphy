(function () {

    'use strict';

    angular
        .module('services')
        .service('user', user);

    /** @ngInject */

    function user($localStorage, $state, toastr) {
        return {
            login                : login,
            logout               : logout,
            registration         : registration,
            getCollection        : getCollection,
            setToCollection      : setToCollection,
            removeFromCollection : removeFromCollection
        };

        function login(data) {
            $localStorage.userHolyGiphy.map(function (item) {
                if (item.username === data) {
                    $localStorage.holyGiphyUser = item;
                }
            });
        }

        function registration(data) {
            $localStorage.userHolyGiphy.push(data);
            $state.go('login');
        }

        function logout() {
            delete $localStorage.holyGiphyUser;
            $state.go('login');
        }

        function getCollection() {
            return $localStorage.holyGiphyUser.gifCollection;
        }

        function setToCollection(data) {
            if (isAvailable($localStorage.holyGiphyUser.gifCollection, data)) {
                toastr.warning('This gif in your collection added earlier!');
            }
            else {
                $localStorage.holyGiphyUser.gifCollection.push(data);
                toastr.success('Good job! Look your collection!');
            }
        }

        function removeFromCollection(data) {
            var collection = $localStorage.holyGiphyUser.gifCollection;
            for (var i = 0; i < collection.length; i++) {
                if (collection[i] === data) {
                    collection.splice(i, 1);
                    toastr.success('Deleted this garbage :)');
                }
            }
        }

        /**
         * Compare elements of the array with second param
         * and return true if was found
         *
         * @param arr
         * @param el
         * @returns {boolean}
         */
        function isAvailable(arr, el) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] === el) {
                    return true;
                }
            }
            return false;
        }
    }
})();