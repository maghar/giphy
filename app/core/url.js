(function () {

    'use strict';

    angular
        .module('url.module', [])
        .factory('url', url);

    /** @ngInject */

    function url() {
        
        var baseUrl = 'http://api.giphy.com/v1/';

        return {
            user: {
                login           :   baseUrl + '',
                registration    :   baseUrl + '',
                userGallery     :   baseUrl + ''
            },
            
            gifs: {
                gifsId          :   baseUrl + 'gifs',
                trendingGifs    :   baseUrl + 'gifs/trending',
                randomGifs      :   baseUrl + 'gifs/random',
                searchGif       :   baseUrl + 'gifs/search',
                uploadGif       :   'http://upload.giphy.com/v1/gifs'
            }
        };
    }
})();